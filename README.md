![XKCDgit.png](https://bitbucket.org/repo/RG5qMq/images/512264661-XKCDgit.png)

# README #

A repository of Learning!

### What is this repository for? ###

* Learning Process of using Java
* Learning Process of using Git
* Learning Process of using Eclipse
* [Learn Markdown Language](https://bitbucket.org/tutorials/markdowndemo)
* [Learn to use Git](https://progit.org/)
* [To Download Eclipse](https://eclipse.org/)

### Where are the notes taken during class? ###

* Check the Wiki page!
* You can use the links to the left
* or [Click Here](https://bitbucket.org/ahought2/cscs1240f16/wiki/)


### Who do I talk to if I have tips or want to fix something?###

* Repo owner or admin
* Email: ahought2@corning-cc.edu
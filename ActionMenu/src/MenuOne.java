import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MenuOne implements ActionListener{
    JLabel yellowLabel;

    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("red")){
            yellowLabel.setText("Red");
            yellowLabel.setBackground(Color.red);
        }
        if(e.getActionCommand().equals("green")){
            yellowLabel.setText("Green");
            yellowLabel.setBackground(Color.green);
        }
        if(e.getActionCommand().equals("blue")){
            yellowLabel.setText("Blue");
            yellowLabel.setBackground(Color.blue);
        }
    }

    private JMenuBar makeMenuBar(){
        JMenuBar greenMenuBar = new JMenuBar();
        greenMenuBar.setOpaque(true);
        greenMenuBar.setBackground(new Color(154, 165, 127));
        greenMenuBar.setPreferredSize(new Dimension(200, 20));

        JMenu colorMenu = new JMenu("Color");
        greenMenuBar.add(colorMenu);

        JMenuItem redChoice = new JMenuItem("Red");
        redChoice.setActionCommand("red");
        redChoice.addActionListener(this);
        colorMenu.add(redChoice);

        JMenuItem greenChoice = new JMenuItem("Green");
        greenChoice.setActionCommand("green");
        greenChoice.addActionListener(this);
        colorMenu.add(greenChoice);

        JMenuItem blueChoice = new JMenuItem("Blue");
        blueChoice.setActionCommand("blue");
        blueChoice.addActionListener(this);
        colorMenu.add(blueChoice);

        JMenuItem orangeChoice = new JMenuItem("Orange");
        orangeChoice.setActionCommand("orange");
        orangeChoice.addActionListener(this);
        colorMenu.add(orangeChoice);
        
        return greenMenuBar;
    }

    private JLabel makeLabel(){
        yellowLabel = new JLabel();
        yellowLabel.setOpaque(true);
        yellowLabel.setBackground(new Color(248, 213, 131));
        yellowLabel.setPreferredSize(new Dimension(200, 180));
        yellowLabel.setText("Yellow");
        return yellowLabel;
    }

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Menu One");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        MenuOne aMenuApp = new MenuOne();
        frame.setJMenuBar(aMenuApp.makeMenuBar());

        frame.getContentPane().add(aMenuApp.makeLabel(), BorderLayout.CENTER);

        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
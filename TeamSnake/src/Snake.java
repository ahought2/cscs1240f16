package frame;

public class Snake {
	
	public Object Snake;
	public int Width;	
	public int Length;
	public int xPos;
	public int yPos;
	public int Appendages;
	
	public Snake(Object o) {
		Snake = o;
		Width = 15;
		Length = 15;
	    xPos = 15;
		yPos = 15;
		Appendages = 1;
	}
}
package Components;

import java.util.ArrayList;

public class Snake {
	public Object Snake;
	public int Width;	
	public int Length;
	public int xPos;
	public int yPos;
	public int Appendages;
	public String Direction;
	public ArrayList<Integer> allX = new ArrayList<Integer>();
	public ArrayList<Integer> allY = new ArrayList<Integer>();
	
	public Snake(Object o) {
		Snake = o;
		Width = 15;
		Length = 15;
	    xPos = 15;
		yPos = 15;
		Appendages = 1;
		Direction = "None";
		allX.add(-500);   //needs a placeholder to start off
		allY.add(-500);   //needs a placeholder to start off
	}
	
	public void Append(){
		this.Appendages++;
		allX.add(this.xPos);  
		allY.add(this.yPos);
	}
}
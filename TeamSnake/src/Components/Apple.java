package Components;

import java.util.Random;

public class Apple {
	public Object Apple;
	public int Width;	
	public int Length;
	public int xPos;
	public int yPos;
	private int[] positions = new int[]{30, 45,60,75,90,105,120,135,150,165,180,195,210,225,240,255,270,285,300,315,330,345,360,375,390,405,420,435,450,465,480,495, 510, 525, 540, 555, 570, 585};
	private Random rand = new Random();

	public Apple(Object o) {
		Apple = o;
		Width = 15;
		Length = 15;
		GenerateNewPosition();
	}
	
	public void GenerateNewPosition(){
	    xPos = positions[rand.nextInt(positions.length)] - Length;
		yPos = positions[rand.nextInt(positions.length)] - Width;
	}
}
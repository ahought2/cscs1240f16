package Frame;

import Components.Apple;
import Components.Snake;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

public class ExtendedPanel extends JPanel implements KeyListener {
	int Score = 0;
    Apple APPLE = new Apple(new Object());
    Snake SNAKE = new Snake(new Object());
	String LastDirection;
	
	public ExtendedPanel() {
		this.setBackground(Color.black);
        addKeyListener(this);
        setFocusable(true);
	}
	
	public Dimension getPreferredSize() {
		Dimension dim = new Dimension(1500, 900);
		return dim;
	}
	
	public void paintComponent(Graphics g) { 	
	 	switch (SNAKE.Direction) {
	 	case "Left": SNAKE.xPos-=15;  
	 		break;
	 	case "Right": SNAKE.xPos+=15;
 			break;
	 	case "Up": SNAKE.yPos-=15;
			break;
	 	case "Down": SNAKE.yPos+=15;
			break;
	 	}
	 	
	 	for (int i = 0; i < SNAKE.Appendages; i++) {
			if (SNAKE.Direction != "None") {
				if (SNAKE.xPos == SNAKE.allX.get(i) && SNAKE.yPos == SNAKE.allY.get(i))  
					System.exit(0);
			}
		}
		
	 	if (SNAKE.Direction != "None")
			SNAKE.Append();
	 	
		for (int i = 0; i < SNAKE.Appendages; i++) {
			if (APPLE.xPos == SNAKE.allX.get(i) && APPLE.yPos == SNAKE.allY.get(i)) {
				APPLE.GenerateNewPosition();
				Score+=20;
			}	
		}
		
		super.paintComponent(g);
		
		g.setColor(Color.green);			
		g.fillRect(SNAKE.xPos, SNAKE.yPos, SNAKE.Width, SNAKE.Length);
		g.setColor(Color.black);	
		g.drawRect(SNAKE.xPos, SNAKE.yPos, SNAKE.Width, SNAKE.Length);
		
		for (int i = 0; i < SNAKE.Appendages; i++) {
			g.setColor(Color.green);			
			g.fillRect(SNAKE.allX.get(i), SNAKE.allY.get(i), SNAKE.Width, SNAKE.Length);
			g.setColor(Color.black);	
			g.drawRect(SNAKE.allX.get(i), SNAKE.allY.get(i), SNAKE.Width, SNAKE.Length);		
		}
		
		g.setColor(Color.red);
		g.fillRect(APPLE.xPos, APPLE.yPos, APPLE.Width, APPLE.Length);
		g.setColor(Color.black);	
		g.drawRect(APPLE.xPos, APPLE.yPos, APPLE.Width, APPLE.Length);	
		
		g.setColor(Color.red);
		if (Score < 99) 
			g.fillRect(265, 0, 75, 20);
		else
			g.fillRect(265, 0, 85, 20);
		g.setColor(Color.white);
		
		g.setFont(new Font("Dialog", Font.BOLD, 15)); 
		g.drawString("Score: " +  Integer.toString(Score),270, 15);	
		
		Update();
	}

    public void Update() {
		try {
			Thread.sleep(80);
		} catch (InterruptedException z) {
			z.printStackTrace();
		}		
		checkCollision();
    }

    public void checkCollision(){
    	if (SNAKE.xPos <= this.getWidth() - 15 && SNAKE.xPos >= 0 && SNAKE.yPos <= this.getHeight() - 15 && SNAKE.yPos >= 0) 
    		repaint();
    	else 
    		System.exit(0);
    }
	
	public void keyPressed(KeyEvent e) {	
	 	LastDirection = SNAKE.Direction; 		
	 	switch (e.getKeyCode()) {
	 	case 37: 
	 		if (LastDirection == "None" || LastDirection == "Left" || LastDirection == "Up" || LastDirection == "Down")
	 			SNAKE.Direction = "Left";
	 		break;
	 	case 38:
	 		if (LastDirection == "None" || LastDirection == "Right" || LastDirection == "Up" || LastDirection == "Left")
	 			SNAKE.Direction = "Up";
 			break;
	 	case 39: 
	 		if (LastDirection == "None" || LastDirection == "Right" || LastDirection == "Up" || LastDirection == "Down")
	 			SNAKE.Direction = "Right";
			break;
	 	case 40: 
	 		if (LastDirection == "None" || LastDirection == "Right" || LastDirection == "Left" || LastDirection == "Down")
	 			SNAKE.Direction = "Down";
	 		break;	
	 	}
	}   
	
	//Unused methods so far, (so they are left empty) but we need to have them because we implement KeyListener 
    public void keyReleased(KeyEvent e) {

    }
    
    public void keyTyped(KeyEvent e) {

    }
}
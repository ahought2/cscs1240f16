package Frame;

import javax.swing.JFrame;

public class ExtendedFrame extends JFrame {
	public static void main(String[] args){
		ExtendedFrame EF = new ExtendedFrame();
		ExtendedPanel EP = new ExtendedPanel();

		EF.setTitle("Snake");
		EF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		EF.setResizable(false);
		EF.add(EP);
		EF.pack();
		EF.setLocationRelativeTo(null);
		EF.setVisible(true);
	}
}
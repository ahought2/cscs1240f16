package frame;

import java.awt.Dimension;
import javax.swing.JFrame;

public class ExtendedFrame extends JFrame {
	
	public static void main(String[] args){
		ExtendedFrame EF = new ExtendedFrame();
		ExtendedPanel EP = new ExtendedPanel();
		
		EF.setPreferredSize(new Dimension(500, 500));
		EF.setTitle("Snake");
		EF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		EF.setResizable(false);
		EF.pack();
		EF.add(EP);
		EF.setVisible(true);
	}
}
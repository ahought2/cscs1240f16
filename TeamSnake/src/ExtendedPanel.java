package frame;

import Components.Apple;
import Components.Snake;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ExtendedPanel extends JPanel implements KeyListener{
	
	boolean UP = false;
	boolean DOWN = false;
	boolean RIGHT = false;
	boolean LEFT = false;
	int Score = 0;
    Apple APPLE = new Apple(new Object());
    Snake SNAKE = new Snake(new Object());
	int lastXPos = SNAKE.xPos;
	int lastYPos = SNAKE.yPos;
	
	public ExtendedPanel(){
		this.setLayout(new FlowLayout());
		this.setBackground(Color.black);
        addKeyListener(this);
        setFocusable(true);
	}
	
	public void paintComponent(Graphics g){
		lastXPos = SNAKE.xPos;
	 	lastYPos = SNAKE.yPos;
		
		if (UP == true) 
			SNAKE.yPos-=15;
		else if (DOWN == true)
			SNAKE.yPos+=15;
		else if (RIGHT == true) 
			SNAKE.xPos+=15;
		else if (LEFT == true)
			SNAKE.xPos-=15;
	 	
		if (APPLE.yPos == SNAKE.yPos && APPLE.xPos == SNAKE.xPos){
			SNAKE.Appendages++;
			Score++;
			APPLE.GenerateNewPosition();
		}
		super.paintComponent(g);
		
		for (int i = 0; i < SNAKE.Appendages; i++){
			g.setColor(Color.green);			
			g.fillRect(SNAKE.xPos, SNAKE.yPos, SNAKE.Width, SNAKE.Length);
			g.setColor(Color.white);	
			g.drawRect(SNAKE.xPos, SNAKE.yPos, SNAKE.Width, SNAKE.Length);
			
			if (UP == true) {
				g.setColor(Color.green);
				g.fillRect(lastXPos, lastYPos - i * 15, SNAKE.Width, SNAKE.Length);	
				g.setColor(Color.white);			
				g.drawRect(lastXPos, lastYPos - i * 15, SNAKE.Width, SNAKE.Length);	
			}
			else if (DOWN == true){
				g.setColor(Color.green);
				g.fillRect(lastXPos, lastYPos + i * 15, SNAKE.Width, SNAKE.Length);	
				g.setColor(Color.white);			
				g.drawRect(lastXPos, lastYPos + i * 15, SNAKE.Width, SNAKE.Length);	
			}
			else if (RIGHT == true) {
				g.setColor(Color.green);
				g.fillRect(lastXPos + i * 15, lastYPos, SNAKE.Width, SNAKE.Length);	
				g.setColor(Color.white);			
				g.drawRect(lastXPos + i * 15, lastYPos, SNAKE.Width, SNAKE.Length);	
			}
			else if (LEFT == true){
				g.setColor(Color.green);
				g.fillRect(lastXPos - i * 15, lastYPos, SNAKE.Width, SNAKE.Length);	
				g.setColor(Color.white);			
				g.drawRect(lastXPos - i * 15, lastYPos, SNAKE.Width, SNAKE.Length);	
			}
		}
		
		g.setColor(Color.red);
		g.fillRect(APPLE.xPos, APPLE.yPos, SNAKE.Width, SNAKE.Length);
		Update();
	}

    public void Update(){
		try {
			Thread.sleep(100);
		} catch (InterruptedException z) {
			z.printStackTrace();
		}		
    	//handles crashing into walls
    	if (SNAKE.xPos <= this.getWidth() && SNAKE.xPos >= 0 && SNAKE.yPos <= this.getHeight() && SNAKE.yPos >= 0)
    		repaint();
    	else {
    		InfoBox("You lose!", true);
    	}	
    }
    
	//Simple method that outputs text to a JFrame
	public static void InfoBox(String Message, boolean terminate){
	    JFrame frame = new JFrame();
	    JOptionPane.showMessageDialog(frame, Message);
	    if (terminate == true)
	    	System.exit(0);
	}
	
	
	public static void CheckCollisions(){
		
	}
	  
	public void keyPressed(KeyEvent e){
		//note to self, use switch statement
		if (e.getKeyCode() == 37){
	    	RIGHT = false;  
	    	LEFT = true;
	        UP = false;
	    	DOWN = false;
	    } else if (e.getKeyCode() == 38){
	    	RIGHT = false;  
	    	LEFT = true;
	        UP = true;
	    	DOWN = false;
	    } else if (e.getKeyCode() == 39){
		    RIGHT = true;  
		    LEFT = false;
		    UP = false;
		    DOWN = false;	
	    } else if (e.getKeyCode() == 40){
	    	RIGHT = false;  
	    	LEFT = true;
	        UP = false;
	    	DOWN = true;
	    }
	}   
	  
	//Unused methods so far, (so they are left empty) but we need to have them because we implement KeyListener 
    public void keyReleased(KeyEvent e){

    }
    
    public void keyTyped(KeyEvent e){

    }
}
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;

public class ExtendedPanel extends JPanel implements MouseMotionListener{
	int rectX = 10;
	int rectY = 10;
	
	public ExtendedPanel(){
	this.addMouseMotionListener(this);
	}
	
	public void paintComponent(Graphics gr){
		super.paintComponent(gr);
//		gr.fillOval(250, 250, 50, 50);
		
		
		gr.fillRect(rectX , rectY, 30, 30);
//		gr.fillRect(15, 15, 15, 15);
	}
	
	public Dimension getPreferredSize(){
		Dimension dim = new Dimension(500, 500);
		return dim;
	}
	
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){
		rectX = e.getX();
		rectY = e.getY();
		repaint();
	}
}

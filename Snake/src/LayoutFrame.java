import javax.swing.JFrame;

public class LayoutFrame {
	public static void main(String[] args){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new LayoutGrid());
		frame.pack();
		frame.setVisible(true);
	}
}

import java.awt.Dimension;
import javax.swing.JFrame;

public class Screen {

    public static void main(String[] args) {
        JFrame fr = new JFrame();
        Board brd = new Board();
        brd.setPreferredSize(new Dimension(100,200));
        fr.add(brd);
        fr.pack();
        fr.setVisible(true);
//      fr.repaint();
    }

}
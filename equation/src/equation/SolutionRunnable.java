package equation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import list.Item;

public class SolutionRunnable implements Runnable {
        public void run() {
        	
            JFrame frame = new JFrame();
            FlowLayout flow = new FlowLayout();
            frame.setLayout(flow);
            
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//            frame.pack();
//            frame.setVisible(true);
         
            Double[] coeffs = {3.7, 82.5, 16.3, -5.723};
       
//          Double[] coeffs = new Double[4];
//          coeffs[0] = 3.7;
//          coeffs[1] = 82.5;
//          coeffs[2] = 16.3;
//          coeffs[3] = -5.723; 
            
            
            Item anItem = new Item(new Term(3.7, new Variable("tre", 37.4), 2.0));
            for(Integer count = 0; count < 3; count++){
            	anItem.insertAfter( new Term(coeffs[count], new Variable("ert", 64.9), 3.0));
            }

            Item bItem = new Item(new Term(8.2, new Variable("tre", 37.4), 2.0));
            for(Integer count = 0; count < 3; count++){
            	bItem.insertAfter( new Term(9.9, new Variable("ert", 64.9), 3.0));
            }
            
            TermLabel coeff;
            TermLabel var;
            String expo;
            TermLabel exp;
            anItem = anItem.first();
            TermLabel plus;
            
            Font aFont = new Font("Tahoma", Font.PLAIN, 32); 
          
            
            do {
            coeff = new TermLabel(((Term)(anItem.item)).coefficient.toString());
        	var = new TermLabel(((Term)(anItem.item)).variable.name);
        	expo = ((Term)(anItem.item)).exponent.toString();
        	exp = new TermLabel("<html><sup>" + expo + "</sup></html>");          
        	plus = new TermLabel("+");
        	var.setFont(aFont);
//        	var.setOpaque(true);
//        	var.addMouseListener(this);
        	exp.setFont(aFont);
//        	exp.setOpaque(true);
//        	exp.addMouseListener(this);
        	coeff.setFont(aFont);
//        	coeff.setOpaque(true);
//        	coeff.addMouseListener(this);
        	plus.setFont(aFont);
//        	plus.setOpaque(true);
//        	plus.addMouseListener(this);
            frame.add(coeff);
            frame.add(var);
            frame.add(exp);
           if( anItem.next != null  ) {frame.add(plus);}
            anItem = anItem.next;
            } while( anItem != null );
            
            TermLabel equals = new TermLabel("=");
            equals.setFont(aFont);
//          equals.setOpaque(true);
//          equals.addMouseListener(this);
            frame.add(equals);

            do {
                coeff = new TermLabel(((Term)(bItem.item)).coefficient.toString());
            	var = new TermLabel(((Term)(bItem.item)).variable.name);
            	expo = ((Term)(bItem.item)).exponent.toString();
            	exp = new TermLabel("<html><sup>" + expo + "</sup></html>");          
            	plus = new TermLabel("+");
            	var.setFont(aFont);
//            	var.setOpaque(true);
//            	var.addMouseListener(this);
            	exp.setFont(aFont);
//            	exp.setOpaque(true);
//            	exp.addMouseListener(this);
            	coeff.setFont(aFont);
//            	coeff.setOpaque(true);
//           	coeff.addMouseListener(this);
            	plus.setFont(aFont);
//            	plus.setOpaque(true);
//            	plus.addMouseListener(this);
                frame.add(coeff);
                frame.add(var);
                frame.add(exp);
               if( bItem.next != null  ) {frame.add(plus);}
                bItem = bItem.next;
                } while( bItem != null );
            frame.pack();
            frame.setVisible(true);
        }
       
}

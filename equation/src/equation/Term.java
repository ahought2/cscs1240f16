package equation;

public class Term {
	Double coefficient = null;
	Variable variable = null;
	Double exponent = null;
		
	
	Term(Double coefficient, Variable vari, Double expo) {
		if(expo == null) {
			exponent = 1.0;	
		} else {
			this.exponent = expo;
		}
		
		
		if(vari == null) { 
			variable = new Variable(null, null);
		} else {
			variable = vari;
		}
		
		
		
		if(coefficient != null) { 
			this.coefficient = coefficient;
		} else {
		 this.coefficient = 1.0;
		}
	}
	
	public Double multiply( ) {
		Double result = null;
		result = coefficient * exponential(variable.value , exponent);
		return result;
	}
	
	Double exponential( Double base, Integer exponent){ return 0.0; }
	
	
	Double exponential( Double base, Double exponent ) {
		Double result = new Double(1.0);
	
//		Integer one = 1;
//		int two = 2;
		
		
		for( Integer counter = new Integer( 0 ); counter < exponent ; counter++) {
		    result = result * base;
		}

		return result;
	}
}
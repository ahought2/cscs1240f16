package equation;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JLabel;

public class TermLabel extends JLabel implements MouseListener {
	public TermLabel(String arg){
		super(arg);
		this.setOpaque(true);
    	this.addMouseListener(this);
    	
	}
	
	 public void mouseClicked(MouseEvent event)  {}
     public void mousePressed(MouseEvent event)  {}
     public void mouseEntered(MouseEvent event)  {
     	if(event.getSource() instanceof TermLabel) {
    	 ((TermLabel)(event.getSource())).setBackground(Color.yellow);
     	}
     }
     public void mouseExited(MouseEvent event)   {
    	 if(event.getSource() instanceof TermLabel){
    	 ((TermLabel)(event.getSource())).setBackground(null);
    	 }
     }
     public void mouseReleased(MouseEvent event) {}
     
}

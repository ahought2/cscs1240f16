package equation;

//import java.text.DecimalFormat;
import list.Item;

public class Solution {
	
	public static void main(String[] args) {
//	Term aTerm = new Term(2.0, new Variable(), 3.0);
	Term aTerm = new Term(2.0, new Variable("x", 2.0), 3.7);
	System.out.println("aTerm: " + aTerm);
	System.out.println("aTerm.exponent: " + aTerm.exponent);
	System.out.println("aTerm.variable: " + aTerm.variable);
	System.out.println("aTerm.variable.name: " + aTerm.variable.name);
	System.out.println("aTerm.variable.value: " + aTerm.variable.value);
	System.out.println("aTerm.coefficient: " + aTerm.coefficient);
	System.out.println( aTerm.multiply());
	
	Polynomial aPoly = new Polynomial();
	
	Integer index = new Integer( 0 );
	while(index < 3 ){
		aPoly.anythingbutTerms[index].coefficient = 2.0 * index;
		System.out.println(aPoly.anythingbutTerms[ index ].multiply() );
		index++;
	}
	
	Item anItem = aPoly.terms.first();
	while(anItem.next != null){
	System.out.println(((Term)(anItem.item)).coefficient);
	anItem = anItem.next;
	}
	System.out.println(((Term)(anItem.item)).coefficient);
	
/*	aTerm.exponent = new Double(3.0);
	aTerm.variable = new Variable();
	aTerm.variable.value = new Double(2.0);
	aTerm.coefficient = new Double(2.0);
	Double check = aTerm.multiply( );
	System.out.println(check);
	
// added on 10/3
	
	Polynomial poly = new Polynomial();
	System.out.println(poly.terms[0].exponent);
	System.out.println(poly.terms[1].exponent);
	System.out.println(poly.terms[2].exponent);
*/	
	}
//	Equation line = null;
//	
//	
//	public static void main(String[] args) {
//		Variable y = new Variable();
//		y.name = new String("y");
//		y.value = new Double(2.0);
//		Term yTerm = new Term();
//		yTerm.variable = y;
//		yTerm.coefficient = new Double(2.0);
//		yTerm.exponent = new Double(2.0);
//		Polynomial yPoly = new Polynomial();
//		yPoly.term0 = yTerm;
//		Equation aLine = new Equation();
//		aLine.leftPoly = yPoly;
//		DecimalFormat form = new DecimalFormat("#.###");
//		System.out.println(form.format(yTerm.coefficient)+ y.name + "^" + form.format(yTerm.exponent));
//	}
}
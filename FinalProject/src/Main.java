import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		readWriteFile("D:/MyFile.txt");	
	}
	
	
	public static void readWriteFile(String fileName) throws IOException {
		FileReader FR = new FileReader(fileName);
        BufferedReader BD = new BufferedReader(FR);
        String line = null;
        PrintWriter PW = new PrintWriter(new FileWriter(fileName, true));
        while ((line = BD.readLine()) != null) {
          System.out.println("Current line contents : " + line); 
          if (line.contains("0") || line.contains("1") || line.contains("2") || line.contains("3")  || line.contains("4") || line.contains("5") || line.contains("6") || line.contains("7") || line.contains("8") || line.contains("9")) {
        	Scanner in = new Scanner(System.in);
    		System.out.println("Enter a number in the console to replace it");
    		int OurNumber = in.nextInt();
    		PW.write(line + System.getProperty("line.separator"));
    		PW.write(Integer.toString(OurNumber));
          }
          else {
          	Scanner in = new Scanner(System.in);
      		System.out.println("Enter a word or letter in the console to replace it");
      		String OurString = in.next();
      		PW.write(line + System.getProperty("line.separator"));
      		PW.write(OurString);
          }
       }
       PW.close();
       BD.close(); 
       System.out.println("Done!");
	}
}